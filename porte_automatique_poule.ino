#include "Arduino.h"
#include "TimeAlarmsmodified.h"
#include <SPI.h>
#include <TimeLib.h>
#include <DS1307RTC.h>
#include <Wire.h>
#include "sunMoon.h"

#define IS_SUMMER 1


#define OUR_latitude 45.873330//reglage de la latitude de chasselay
#define OUR_longtitude 4.770660//reglage de la longitude de chasselay
#define OUR_timezone 60//reglage du fuseau horaire de la france par rapport a utc

sunMoon  sm;//bibliotheque pour le calcule de couchée de soleil


//The setup function is called once at startup of the sketch
int bouton = 2;
bool boutonState;

bool isopen = 1;
int portepin = 8;
void setup()
{
	Serial.begin(9600);
	settime();
	setalarme();
  setclosealarm();
	setbouton();
	setporte();
}


// The loop function is called in an endless loop
void loop()
{
   applyState();
	saytime();
	testbouton();
	Alarm.delay(1000); // wait one second between clock display
}






//gestion de l'heure

void settime(){//regle l'heure
   setSyncProvider(rtcdate); //regle l'heure avec l'aide d'un serveur ntp
   //setTime(1620061402); //regle l'heure manuellement
   sm.init(OUR_timezone, OUR_latitude, OUR_longtitude);//initialisation pour le calcule du soleil
  }

unsigned long rtcdate(){
	return (RTC.get());
}

//fonction qui retourne l'heure de couchée de soleil du jour
time_t getsunset(){
  return sm.sunSet(0, IS_SUMMER);//on retourne le couchée de soleil
}

void saytime(){
		  Serial.print(hour());
		  digit(minute());
		  digit(second());
		  Serial.print(" ");
		  Serial.print(day());
		  Serial.print(" ");
		  Serial.print(month());
		  Serial.print(" ");
		  Serial.print(year());
		  Serial.println();
}

void digit(int digits){
	// utility function for digital clock display: prints preceding colon and leading 0
	  Serial.print(":");
	  if(digits < 10)
	    Serial.print('0');
	  Serial.print(digits);
}





// gestion des alarme
void setalarme(){//pour regler les plage horaire


    Alarm.alarmRepeat(12,00,00,setclosealarm);//on calcul puis regle l'heure de fermeture tout les midi
  
		Alarm.alarmRepeat(dowMonday,7,30,00,open);  
    Alarm.alarmRepeat(dowTuesday,7,30,00,open);  
    Alarm.alarmRepeat(dowWednesday,7,30,00,open);  
    Alarm.alarmRepeat(dowThursday,7,30,00,open);  
    Alarm.alarmRepeat(dowFriday,7,30,00,open);   

    Alarm.alarmRepeat(dowSaturday,8,30,00,open);  
    Alarm.alarmRepeat(dowSunday,8,30,00,open);


    
    
		Serial.println("alarme set");
}

void setclosealarm(){
  time_t sunset = getsunset();//on stocke l'heure de fermeture
  Serial.print("l'heure de fermeture sera: ");
  Serial.print(hour(sunset));
  Serial.print("h ");
  Serial.print(minute(sunset));
  Serial.print("min ");
  Serial.print(second(sunset));
  Serial.println("sec ");

  Alarm.alarmOnce(hour(sunset),minute(sunset),second(sunset),close);//on regle l'heure
}



//gestion des fermeture ouverture

void setporte(){
	pinMode(portepin, OUTPUT );
}

void open(){
  isopen=1;
}

void close(){
  isopen=0;
}

void applyState(){
  digitalWrite(portepin, isopen);
  Serial.print("apply : ");
  Serial.println(isopen);
}



//gestion des bouton

void testbouton(){
	if (digitalRead(bouton) != boutonState) {
  boutonState=digitalRead(bouton);
		if (isopen){
		  close();
		 }else if (!isopen){
      open();
		 }
	}
}

void setbouton(){
	pinMode(bouton, INPUT);
 boutonState=digitalRead(bouton);
	}
